#!/bin/sh
nitrogen --restore &
picom &
volumeicon &
lxqt-policykit-agent &
light-locker &
xautolock -locker "light-locker-command -l" -time 5 -corners "00+-" -cornerdelay 2 &
deja-dup &
